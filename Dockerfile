# Build in Docker container
FROM golang:1.13 as builder

RUN update-ca-certificates

ENV CGO_ENABLED 0
WORKDIR /src/s3sync
COPY . ./
RUN go mod vendor && \
    go build -o s3sync ./cli

# Create s3sync image
FROM scratch
COPY --from=builder /src/s3sync/s3sync /s3sync
# Copy the ca-certificate.crt from the builder stage
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["/s3sync"]
